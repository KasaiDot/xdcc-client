var gulp = require('gulp'),
	ts = require('gulp-typescript'),
	livereload = require('gulp-livereload'),
	exec = require('child_process').exec;

var tsProject = ts.createProject('tsconfig.json');

gulp.task('run', function () {
	return exec('"node_modules/electron-prebuilt/dist/electron" main.js');
})

gulp.task('watch', function () {
	livereload.listen()
	gulp.watch('src/**/*.tsx', ['tsc']);
});

gulp.task('tsc', function () {
	return tsProject.src()
        .pipe(ts(tsProject))
		.pipe(gulp.dest("."))
		.pipe(livereload())
});

gulp.task('default', ['tsc', 'run', 'watch'])