export class XdccFinder {

	private apiUrl: string = "http://ixirc.com/api/?q=";

	async search(query: string): Promise<XdccResult[]> {
		let mapped = []
		try {
			let res = await window.fetch(this.apiUrl + query)
			let json = await res.json()

			if (json.results != null) {
				mapped = json.results.map(item => new XdccResult(item.pid, item.name, item.nname, item.naddr, item.nport, item.cname, item.uname, item.age, item.szf, item.n, item.lastf));
			}
		} catch (err) {
			console.log("error searching ", err)
		}
		return mapped;
	}
}


export class XdccResult {

	constructor(
		public id: number,
		public title: string,
		public networkName: string,
		public networkAddress: string,
		public port: number,
		public channel: string,
		public bot: string,
		public age: number,
		public fileSize: string,
		public packNr: number,
		public lastUpdated: string
	) { }

}