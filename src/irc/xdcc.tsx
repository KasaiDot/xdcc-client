import * as net from "net"
import * as fs from "fs"
import {EventEmitter} from "events"
import {Client} from "irc"
import * as Immutable from "immutable"
import {FileDownloader, FileInfo} from './file-downloader'


interface PackInfo {
	command: string
	filename: string
	ip: string
	port: number
	filesize: number
	fileLocation?: string
	resumepos: number
	resume?: boolean
}


export class Request {
	constructor(public nick: string, public pack: number, public path: string, public resume = true) { }
}


export class DccDownloader extends EventEmitter {


	private finished = false;
	private packInfo: PackInfo;
	private filedownloader: FileDownloader;


	constructor(private client: Client, private args: Request) {
		super();

		this.killrequest.bind(this)
		//init events
		this.on("kill", () => {
			this.killrequest()
		});
		this.on("cancel", () => {
			if (this.finished) {
				return;
			}
			// Cancel the pack
			this.client.say(this.args.nick, "XDCC CANCEL");
			this.killrequest();
		});

		this.client.on("ctcp-privmsg", this.ctcpHandler);
	}


	startDownload() {
		this.client.say(this.args.nick, "XDCC SEND " + this.args.pack);
	}


	private handleAccept = (params: Array<any>) => {
		console.log("handleAccept", params)
		// Check accept command params
		if (this.packInfo.filename == params[2] &&
			this.packInfo.port == parseInt(params[3], 10) &&
			this.packInfo.resumepos == parseInt(params[4], 10)) {
			// Download the file
			this.packInfo.command = params[1];

			this.downloadFile(this.packInfo)
		}
	}
	

	private downloadFile(pack: PackInfo){
		this.filedownloader = FileDownloader.download(new FileInfo(pack.port, pack.ip, pack.filesize, pack.fileLocation, pack.resumepos));
		this.filedownloader.on("progress", (pack, received) => this.emit("progress", pack, received))
	}

	private handleSend = (params: Array<any>) => {
		console.log("handleSendMsg", params)
		this.packInfo = {
			command: params[1],
			filename: params[2],
			ip: this.intToIp(parseInt(params[3], 10)),
			port: parseInt(params[4], 10),
			filesize: parseInt(params[5], 10),
			resumepos: 0
		}
		// Get the download location
		this.packInfo.fileLocation = this.args.path + (this.args.path.substr(-1, 1) == "/" ? "" : "/") + this.packInfo.filename;
		// Check for file existence
		fs.stat(this.packInfo.fileLocation + ".part", (err, stats) => {
			// File exists
			if (!err && stats.isFile()) {
				if (this.args.resume) {
					// Resume download
					this.client.ctcp(this.args.nick, "privmsg", "DCC RESUME " + this.packInfo.filename + " " + this.packInfo.port + " " + stats.size);
					this.packInfo.resumepos = stats.size;
				} else {
					// Dont resume download delete .part file and start download
					fs.unlink(this.packInfo.fileLocation + ".part", (err) => {
						if (err) throw err;
						this.downloadFile(this.packInfo)
					});
				}
			} else {
				// File dont exists start download
				this.downloadFile(this.packInfo)
			}
		});
	}

	ctcpHandler = (sender, target, message) => {
		console.log("ctcpHandler", message)
		if (this.finished) {
			return;
		}
		//handle DCC massages
		if (sender === this.args.nick && target === this.client.nick && message.substr(0, 4) == "DCC ") {
			// Split the string into an array
			// The String format is as follows:
			// DCC {command} ("|'){filename}("|') {ip} {port}( {filesize})
			var parser = /DCC (\w+) "?'?(.+?)'?"? (\d+) (\d+) ?(\d+)?/;
			var params = message.match(parser);

			if (params != null) {
				switch (params[1]) {
					//Got DCC SEND message
					case "SEND":
						this.handleSend(params)
						break;
					// Got DCC ACCEPT message (bot accepts the resume command)
					case "ACCEPT":
						this.handleAccept(params)
						break;
				}
			}
		}
	}

	// kill this request dont react on further messages
	public killrequest() {
		this.finished = true;
		this.packInfo = null;
		this.filedownloader.stopDownload()
	}

	private intToIp(n: number): string {
		return Immutable.Range(0, 25, 8)
			.reverse()
			.map(shift => (n >> shift) & 255)
			.join(".")
	}


}