
import * as fs from "fs"
import * as net from "net"
import {EventEmitter} from "events"



export class FileInfo {
	constructor(
		public port: number,
		public ip: string,
		public filesize: number,
		public fileLocation: string,
		public resumepos: number
	) { }
}


export class FileDownloader extends EventEmitter {

	progressInterval: NodeJS.Timer;


	public static download(pack: FileInfo): FileDownloader {
		let dler = new FileDownloader()
		// Write stream to store data
		let stream = fs.createWriteStream(pack.fileLocation + ".part", { flags: 'a' });

		stream.on("open", () => {
			let sendBuffer = new Buffer(4)
			let received = pack.resumepos
			let ack = pack.resumepos

			// Open connection to the bot
			let conn = net.connect(pack.port, pack.ip, () => {
				dler.emit("connect", pack);
				dler.progressInterval = setInterval(() => {
					let progress = received / pack.filesize * 100;
					dler.emit("progress", pack, progress, received);
				}, 500)
			});

			// Callback for data
			conn.on("data", (data) => {
				received += data.length;
				//support for large files
				ack += data.length;
				while (ack > 0xFFFFFFFF) {
					ack -= 0xFFFFFFFF;
				}

				sendBuffer.writeUInt32BE(ack, 0);
				conn.write(sendBuffer);

				stream.write(data);
			});

			// Callback for completion
			conn.on("end", () => {
				dler.emit("progress", pack, 100, received);
				// End the transfer
				clearInterval(dler.progressInterval)
				stream.end();
				// Connection closed
				if (received == pack.filesize) {// Download complete
					fs.rename(pack.fileLocation + ".part", pack.fileLocation);
					dler.emit("complete", pack);
				} else if (received != pack.filesize) {// Download abborted
					dler.emit("dlerror", pack, "Server closed connection, download canceled");
				}
				conn.destroy();
			});

			// Add error handler
			conn.on("error", (error) => {
				clearInterval(dler.progressInterval)
				// Close writestream
				stream.end();
				// Send error message
				dler.emit("dlerror", pack, error);
				// Destroy the connection
				conn.destroy();
			});

			dler.on("stop", () => {
				stream.end()
				conn.destroy()
				clearInterval(dler.progressInterval)
			})
		});
		stream.on("error", (error) => {
			// Close writestream
			clearInterval(dler.progressInterval)
			stream.end();

			dler.emit("dlerror", pack, error);
		});
		return dler;
	}

	stopDownload() {
		this.emit("stop")
	}


}