import {Client} from "irc"
import {DccDownloader} from "irc/xdcc"


export class IrcLogger {

	static bind(client: Client, downloader?: DccDownloader): void {
		client.on("kick", (nick, by, reason, message) =>
			this.log("You were kicked by " + by + ". Reason: " + reason)
		);
		client.on("error", (message) => this.log("error occured: ", message));

		client.on("registered", (r) => {
			this.log("registered, msg:  ", r)
		})

		client.on("join", (channel, nick, message) => {
			if (nick === client.nick) {
				this.log("joined " + channel)
			};
		})

		client.on("quit", (nick, reason, channels, message) => {
			if (nick === client.nick) {
				this.log("You were disconnected: " + reason)
			}
		})

		client.on("ctcp-privmsg", (from, to, text) => {
			this.log("[CTCP] " + from + ": " + text);
		})

		client.on("invite", (channel, from) => {
			this.log(from + " invited you to " + channel)
		})

		client.on("topic", (channel, topic, msg) => {
			this.log("[TOPIC]" + channel + ": " + topic);
		})
		
		client.on("ctcp-version", (from, to, message) => {
			this.log(from + " -> [CTCP-VERSION]", message)
		})
		
		// client.on("raw", (msg) => {
		// 	this.log("raw message", msg)
		// })

		if (downloader != null) {
			downloader.on("dlerror", (pack, msg) => console.log(msg))
			downloader.on("connect", (msg) => console.log(msg))
			downloader.on("progress", (pack, received) => {
				let progress = Math.floor(received / pack.filesize * 1000);
			//	console.log("-- " + progress + "% DONE " + pack.fileLocation);
			})
			downloader.on("complete", (pack) => console.log("pack complete ", pack.fileLocation))
		}
	}

	static log(msg: string, ...args: any[]) {
		console.log(msg, args);
	}


}