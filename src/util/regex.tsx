import * as Immutable  from "immutable"

export class Regex {

	constructor(private regex: RegExp) { }

	getAllMatches(str: string): Immutable.List<string> {
		return Immutable.Range(0)
			.map(() => this.regex.exec(str))
			.takeWhile(r => r != null)
			.map(r => r[0])
			.toList()
	}

}