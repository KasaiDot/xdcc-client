import * as Immutable from 'immutable'

export function generateString(length: number): string {
    let alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    return Immutable.Range(0, length)
        .map(() => alphabet.charAt(Math.floor(Math.random() * alphabet.length)))
        .join("")
}