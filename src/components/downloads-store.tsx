import {List} from "immutable"
import {EventEmitter} from "events"


export module ProgressStore {

	const events = new EventEmitter()
	const progressMap = new Map<String, ProgressInfo>()

	export function updateProgress(id: string, progress: ProgressInfo) {
		console.log("updateProgress", progress)
		progressMap.set(id, progress)
		events.emit("progress", progress)
	}

	export function getProgressList(): Array<ProgressInfo> {
		return Array.from(progressMap.values());
	}

	export function onProgress(listener: (progress: ProgressInfo) => void) {
		console.log("onProcess")
		events.addListener("progress", listener)
	}

	export interface ProgressInfo {
		progress: number,
		name: string
	}
}

