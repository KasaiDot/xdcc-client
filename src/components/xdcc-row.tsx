import * as React from 'react';
import {XdccResult} from './search/search-api'
import {Client} from 'irc'
import * as Random from 'util/random'
import {List} from 'immutable'
import {Regex} from 'util/regex'
import {IrcLogger} from '../irc/irc-logger'
import {DccDownloader, Request} from '../irc/xdcc'
import {ProgressStore} from "./downloads-store"



interface ItemProps {
	xdccItem: XdccResult;
}


export class XdccRow extends React.Component<ItemProps, {}> {

	style = {
		cursor: "hand"
	}

	constructor(props: ItemProps) {
		super(props);
	}

	render() {
		return <div style={ this.style } onClick={ this.handleClick }>
					<div>{this.props.xdccItem.title} - {this.props.xdccItem.fileSize}</div>
					<div>{this.props.xdccItem.networkName}[{this.props.xdccItem.channel}]</div>
			</div>
	}

	handleClick = (e) => {
		let item = this.props.xdccItem;
		console.log("downloading item")

		let username = Random.generateString(8);
		console.log("generated username " + username)
		let client = new Client(item.networkAddress, username, {
			port: item.port,
			realName: username,
			channels: [item.channel],
			autoConnect: false
		})

		let downloader = new DccDownloader(client, new Request(item.bot, item.packNr, "F:/develop/"))
		client.on("join", (channel, nick, msg) => {
			if (nick === client.nick) {
				console.log("joined channel")
				downloader.startDownload();
			}
		})
		downloader.on("progress", (pack, progress) => ProgressStore.updateProgress(pack.fileLocation, { name: pack.fileLocation, progress: progress }))
		downloader.on("complete", (pack) => {
			client.disconnect()
			ProgressStore.updateProgress(pack.fileLocation, { name: pack.fileLocation, progress: 100 })
		})

		IrcLogger.bind(client, downloader)
		client.connect()
		console.log(client)
	}
}