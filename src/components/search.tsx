import * as React from 'react'
import {XdccFinder, XdccResult} from 'search/search-api';
import {XdccRow} from './xdcc-row'


interface SearchState {
	resultList: Array<XdccResult>;
}


export class SearchComponent extends React.Component<{}, SearchState> {

	searchboxStyle = {
		"marginTop": 7,
		"height": 20,
	}


	finder = new XdccFinder();
	state: SearchState = { resultList: [] }

	componentDidMount() {
		this.search("test");
	}

	render() {
		let searchbox = <input style = { this.searchboxStyle } type="text" className="col-md-2" onKeyDown={this.handleSearchValueChange}/>
		let xdccResults = this.state.resultList.map(r => <li className="list-group-item" key= {r.id}><XdccRow xdccItem = {r}/> </li>)

		return <div>
					<div>{searchbox}</div>
					<ul className="list-group col-md-12">{xdccResults}</ul>
			</div>
	}

	handleSearchValueChange = (event) => {
		if (event.key === "Enter") {
			console.log(event.target.value)
			this.search(event.target.value)
		}
	}

	search(query: string) {
		this.finder.search(query)
			.then(response => {
				this.setState({ resultList: response })
			})
			.catch(e => console.log("ERROR " + e))
	}

}