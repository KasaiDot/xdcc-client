import * as React from 'react'
import {ProgressStore} from "./downloads-store"
import {Button, Tabs, Tab, ProgressBar} from "react-bootstrap"


interface DownloadState {
	downloadList: Map<String, ProgressStore.ProgressInfo>
}

export class DownloadComponent extends React.Component<{}, DownloadState> {

	state = { downloadList: new Map() }

	constructor() {
		super();

		ProgressStore.onProgress((info) => {
			console.log(info)
			this.state.downloadList.set(info.name, info)
			this.forceUpdate()
		})
	}

	render() {
		console.log("downloadviewstate render")
		let downloadList = Array.from(this.state.downloadList.values()).map(dl => {
			// console.log("map ", dl)
			return <div key = {dl.name}>
						<div>{dl.name}</div>
						<ProgressBar style={{ height: 16 }} bsSize="lg" now={dl.progress} max={100}></ProgressBar>
					</div>
		})
		console.log("dllist", downloadList)
		return <div>{downloadList}</div>
	}


}