import * as React from 'react'
import * as ReactDOM from 'react-dom'
import {SearchComponent} from './components/search'
import * as Immutable from 'immutable'
import {DownloadComponent} from "components/downloads"
import {Button, Tabs, Tab, ProgressBar} from "react-bootstrap"



class App extends React.Component<{}, {}> {

	render() {
		Log.info("test")
		return (
			<Tabs>
			<Tab eventKey={1} title="Search"><SearchComponent>Test</SearchComponent></Tab>
			<Tab eventKey={2} title="Downloads"><DownloadComponent/></Tab>
				</Tabs>
		)
	}

}



export module Log {

	enum LogLevel {
		DEBUG = 1,
		INFO = 2,
		ERROR = 3
	}

	let LOG_LEVEL = LogLevel.INFO

	export function debug(msg: string, ...args: any[]) {
		if (LOG_LEVEL <= LogLevel.DEBUG) {
			console.log(msg, args)
		}
	}

	export function info(msg: string, ...args: any[]) {
		if (LOG_LEVEL <= LogLevel.INFO) {
			console.log(msg, args)
		}
	}

	export function error(msg: string, ...args: any[]) {
		if (LOG_LEVEL <= LogLevel.ERROR) {
			console.error(msg, args)
		}
	}


}

ReactDOM.render(React.createElement(App), document.getElementById('app'));