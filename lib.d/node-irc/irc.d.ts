declare module 'irc' {
  import * as net from "net"
  import {EventEmitter} from "events"

  export class Client extends EventEmitter {
    nick: string;
    constructor(server: string, nick: string, opt: ClientOptions);
    connect();
    disconnect(message?: string);
    on(eventName: string, handler: (...args: any[]) => void);
    say(target: string, text: string);
    join(channel: string);
    conn: net.Socket;
    ctcp(target: string, type: string, text: string);
  }

  interface ClientOptions {
    port: number,
		realName: string,
		username?: string,
		autoConnect?: boolean,
    debug?: boolean,
    showErrors?: boolean,
    autoRejoin?: boolean,
    channels?: Array<String>,
    secure?: boolean,
    stripColors?: boolean,
    encoding?: string,
    [propertyName: string]: any;
  }

  interface Colors {
    codes: { [colorName: string]: string };
  }

  export var colors: Colors;
}